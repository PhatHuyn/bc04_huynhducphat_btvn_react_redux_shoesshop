import { DataShoes } from "../../Data_Shoes/Data_Shoes";
import { GIAM_SO_LUONG, TANG_SO_LUONG, THEM_SAN_PHAM, XEM_CHI_TIET, XOA_SAN_PHAM } from "../shoeConstants/shoeConstants";

let initialState = {
    shoesArray: DataShoes,
    detailShoes: DataShoes[0],
    gioHangShoes: [],
};

export let shoeReducer = (state = initialState, action) => {
    switch(action.type){
        case XEM_CHI_TIET: {
            state.detailShoes = action.payload;
            return { ...state};
        }
        case THEM_SAN_PHAM: {
            let clonegioHang = [ ...state.gioHangShoes];
            let shoes = action.payload;
            let index = state.gioHangShoes.findIndex((item) => {
                return item.id == shoes.id;
            });
            if(index == -1){
                let spGioHang = { ...shoes, soLuong: 1};
                clonegioHang.push(spGioHang);
            }else{
                clonegioHang[index].soLuong++;
            }
            state.gioHangShoes = clonegioHang;
            return { ...state};
        }
        case GIAM_SO_LUONG: {
            let clonegioHang = [...state.gioHangShoes];
            let idshose = action.payload;
            let index = state.gioHangShoes.findIndex((item) => {
                return item.id == idshose;
            });
            if(state.gioHangShoes[index].soLuong == 1){
                console.log("không thể giảm");
            }else{
                clonegioHang[index].soLuong--;
            }
            state.gioHangShoes = clonegioHang;
            return{ ...state};
        }
        case TANG_SO_LUONG: {
            let clonegioHang = [...state.gioHangShoes];
            let idshose = action.payload;
            let index = state.gioHangShoes.findIndex((item) => {
                return item.id == idshose;
            });
            clonegioHang[index].soLuong++;
            state.gioHangShoes = clonegioHang;
            return{ ...state};
        }
        case XOA_SAN_PHAM: {
            let clonegioHang = [...state.gioHangShoes];
            let idshose = action.payload;
            let index = state.gioHangShoes.findIndex((item) => {
                return item.id == idshose;
            });
            clonegioHang.splice(index, 1);
            state.gioHangShoes = clonegioHang;
            return{ ...state};
        }
        default:
            return state;
    }
};
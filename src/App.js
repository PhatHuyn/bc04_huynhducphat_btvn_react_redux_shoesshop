import logo from './logo.svg';
import './App.css';
import ShoesStore_Redux from './ShoesStore_Redux/ShoesStore_Redux';

function App() {
  return (
    <div className="App">
      <ShoesStore_Redux />
    </div>
  );
}

export default App;

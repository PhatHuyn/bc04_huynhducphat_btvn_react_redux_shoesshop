import React, { Component } from 'react'
import ItemShoes from './ItemShoes'
import { connect } from "react-redux";

class ListShoes extends Component {
  render() {
    return (
        <div className='container'>
          <div className='row'>
            {this.props.data.map((item, index) => {
              return(
                <div key={index} className='col-3'>
                  <ItemShoes                   
                  detail={item}
                  />
                </div>
              );
            })}
          </div>
        </div>
    );
  }
}
let mapStateToProps = (state) =>{
  return{
    data: state.shoeReducer.shoesArray,
  };
};

export default connect(mapStateToProps, )(ListShoes);
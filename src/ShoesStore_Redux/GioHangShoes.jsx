import React, { Component } from 'react'
import { connect } from "react-redux";
import { GIAM_SO_LUONG, TANG_SO_LUONG, XOA_SAN_PHAM } from '../redux/shoeConstants/shoeConstants';

class GioHangShoes extends Component {
  renderTbody = () => {
    return this.props.giohang.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            {" "}
            <img src={item.image} style={{ width: 80 }} alt="" />
          </td>
          <td>
            <button 
            onClick={() => { 
              this.props.handleTru(item.id)
             }} 
             className="btn btn-primary">-
            </button>
            <span className="mx-3">{item.soLuong}</span>
            <button
            onClick={() => { 
              this.props.handleCong(item.id)
             }} 
             className="btn btn-success">+
             </button>
          </td>
          <td>
            <button
            onClick={() => { 
              this.props.handleRemove(item.id)
             }} 
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    console.log("gio hang", this.props.giohang);
    return (
      <div className="container py-5">
        {/* gioHang */}
        <table className="table text-left">
          <thead>
            <tr>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
        {this.props.giohang.length == 0 && (
          <p className=" mt-5 text-center">Chưa có sản phẩm trong giỏi hàng</p>
        )}
      </div>
    )
  }
}

let mapStateToProps = (state) =>{
  return{
    giohang: state.shoeReducer.gioHangShoes,
  };
};
let mapDispatchToProps = (dispatch) => {
  return{
    handleTru: (value) => {
      dispatch({
        type: GIAM_SO_LUONG,
        payload: value, 
      });
    },
    handleCong: (value) => {
      dispatch({
        type: TANG_SO_LUONG,
        payload: value, 
      });
    },
    handleRemove: (value) => {
      dispatch({
        type: XOA_SAN_PHAM,
        payload: value, 
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GioHangShoes);
import React, { Component } from 'react'
import { DataShoes } from '../Data_Shoes/Data_Shoes';
import DetailShoes from './DetailShoes';
import GioHangShoes from './GioHangShoes';
import ListShoes from './ListShoes';

export default class ShoesStore_Redux extends Component {

  render() {
    // console.log("gio hang", this.state.gioHangShoes);
    return (
      <div>
        <GioHangShoes />
        <ListShoes />
        <DetailShoes />
      </div>
    )
  }
}

